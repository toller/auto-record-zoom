# Auto Zoom Recorder: A Powershell Script
Auto Zoom Recorder is a Powershell script that pieces together other software to allow you to automatically join and record Zoom meetings on Windows based on your calendar.

## Disclaimer
**Please be aware of your local recording and wiretapping laws.** The author of this software accepts no responsibility or liability for its use. Local laws vary, so it is up to the user to determine what laws are applicable to their use of this software. You are responsible for gaining the express permission needed of the relevant parties when recording.

If needed for your jurisdiction, you can disable the automatic recording feature by removing `-Args "--startrecording"` from the OBS launch call. This will still launch OBS, but the user will need to click "Start Recording" in OBS.

## Features
* Automatically join and record Zoom meetings using OBS
* Stops recording automatically 
* Renames recordings based on the meeting title and start time

## Requirements
Auto Zoom Recorder needs the following tools set-up before its use:

* Zoom
* For recording: [OBS Studio](https://obsproject.com/)
* For launching meetings: [Zoom-Go](https://github.com/benbalter/zoom-go)
* OPTIONAL, for randomizing your Zoom name: [PSSQLite](https://github.com/RamblingCookieMonster/PSSQLite)

## Setup
### Setting up OBS
1. Download and install OBS
2. Join a test Zoom meeting https://zoom.us/test
3. Launch OBS
4. Click + under Sources
5. Choose "Window Capture"
6. Select the Zoom Window with title "Zoom Meeting"
7. Confirm that it captures your window
8. Under File-->Settings-->Output, set and note your recording path

If all is well, you're set! You should see your Zoom window within OBS, and note that it's capturing audio as well from the test call.

### Setting up Zoom-Go
Additionally, just install go and follow the install instructions there. The latest release (as of this writing 2020-05-04) is buggy. Do not use the release as this will not work properly with it (no joining of Zoom rooms with passwords, most importantly). You will need to compile from source following their instructions.

As for actual setup of the tool: I'd recommend creating a separate Google account thats calendar is only meetings that you want to record. Note that they must actually be on the Google account's calendar, not just an additional calendar you're viewing.

To test, run `zoom` from the command line and you should see your next Zoom meeting's details show up.

### Setting up this script
1. Set up the requirements above and verify they are working (I recommend putting PSSQLite as a subfolder next to the script)
2. Edit constants $C_PSSQLITE_PATH, $C_OBS_PATH, $C_OBS_EXE, $C_OUTPUT_PATH depending on where you installed to and your OBS setup ($C_OUTPUT_PATH is the recording path set when setting up OBS)
3. Use Task Scheduler to set-up a repeat event that runs this script

For the interval, I recommend starting at 3 minutes before the hour (00:57) and running at a 15 minute interval. This way, you join meetings before they officially start, or shut them off at a reasonable amount of time after it ends without taking up too much additional disk space.

Note that this script records all audio on the computer it's running when a Zoom meeting is happening. Additionally, as it will join meetings automatically, make sure your audio input, output, and video devices are set up on a call before you start running this script. The author is also not responsible for you embarassing yourself!

### Randomizing name on join
This script optionally ($C_CHANGE_NAME) can load up a list of names to randomly use when joining a Zoom Meeting. This is more for fun more than anything else.

To set it up, make a plaintext file named "zoom_names.txt" next to this script. Each line is an encoded name from Zoom. You'll have to populate it manually by setting a name in Zoom, joining a meeting, and pulling it from zoomus.db using your favorite SQLite browser. It's annoying but I couldn't figure out how Zoom encodes names in zoomus.db and doing so was outside of the scope of this project. 
