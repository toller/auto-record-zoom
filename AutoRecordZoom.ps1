﻿# Define Constants
$C_PSSQLITE_PATH = ".\PSSQLite"
$C_ZOOM_CAL_EXE = "zoom" #zoom-go EXE, in your path or gopath
$C_ZOOM_EXE =    "zoom" 
$C_ZOOM_WINDOW_TITLES = "Zoom Meeting", "Waiting for Host"
$C_ZOOM_DB_PATH = "$($env:APPDATA)\Zoom\data\zoomus.db"
$C_OBS_PATH = "C:\Program Files\obs-studio\bin\64bit" #install path of OBS
$C_OBS_MAIN_EXE = "obs64"
$C_OBS_ENC_EXE = "obs-ffmpeg-mux"
$C_DATE_FORMAT = "yyyy-MM-dd HH-mm"
$C_OUTPUT_PATH = ""  # folder OBS saves to, include trailing slash
$C_OUTPUT_TYPE = "mkv" # file time OBS is saving as
$C_NAMES_TXT = ".\zoom_names.txt"
$C_CHANGE_NAME = $false
$C_LOG_FILENAME = ".\recording_log.txt"

function Set-RandomZoomUsername {
    # Pull the encoded names from $C_NAMES_TXT
    Import-Module $C_PSSQLITE_PATH
    $hashed_name = Get-Random -InputObject (Get-Content $C_NAMES_TXT)
    $Query = "UPDATE zoom_kv SET value = `"{0}`" WHERE key = `"com.zoom.client.saved.username.forjoin.enc`"" -f $hashed_name
    Invoke-SqliteQuery -DataSource $C_ZOOM_DB_PATH -Query $Query
}

# Program start
# Check if we're in the process of recording a Zoom meeting
$zoom = Get-Process -ErrorAction SilentlyContinue -Name $C_ZOOM_EXE | Where-Object {$_.mainWindowTitle -in $C_ZOOM_WINDOW_TITLES}
$obs = Get-Process -ErrorAction SilentlyContinue -Name $C_OBS_MAIN_EXE
$exit = $false

if ($zoom -and $obs) {
    "Meeting in progress, will exit"
    $exit = $true
} elseif (-not $zoom -and $obs) {
    "Zoom meeting ended, stopping recording"
    Stop-Process -Name $C_OBS_MAIN_EXE

    # Wait for the ffmpeg process to end before trying to rename the file
    Wait-Process -Name $C_OBS_ENC_EXE

    $meeting_name = Get-Item ($C_LOG_FILENAME) | Get-Content -Tail 1

    Get-ChildItem -Path $C_OUTPUT_PATH -Filter ("*" + $C_OUTPUT_TYPE) | 
        Sort LastWriteTime | 
        Select-Object -Last 1 | 
        Rename-Item -NewName ($meeting_name + '.' + $C_OUTPUT_TYPE)

    # Set a new name for next Zoom, if needed
    if ($C_CHANGE_NAME) { Set-RandomZoomUsername }
}

# clear the vars used
Remove-Variable -name zoom
Remove-Variable -name obs

# at this point we have handled both cases if something is currently running. Time to check if we need to run a new one
if (-not $exit) {
    "Checking for Zoom meeting"
    $zl_output = cmd /c $C_ZOOM_CAL_EXE

    if ($zl_output[-1] -match 'Opening') {
	    # we started a new meeting
        "Starting to record"
        Start-Process -WorkingDirectory $C_OBS_PATH -FilePath $C_OBS_MAIN_EXE -Args "--startrecording"

        # prep the meeting name, save to file
        Remove-Variable -Name matches
        $zl_output[0] -match '".*"' | Out-Null
        $meeting_name = ($matches[0] -replace '"', '') + ' ' + (Get-Date -Format $C_DATE_FORMAT)
        Add-Content -Path ($C_LOG_FILENAME) -Value $meeting_name
        Remove-Variable -Name matches
    }
}
"Exiting"
Remove-Variable -name exit
